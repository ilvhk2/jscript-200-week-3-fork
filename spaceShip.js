// 1. Create a class function SpaceShip
// - should set two properties: name and topSpeed
// - should have a method accelerate that logs to the console 
//   `${name} moving to ${topSpeed}`

class SpaceShip {
    constructor(n, t,){
        this.name = n;
        this.topSpeed = t;
    }
    accelerate (){
        const {name, topSpeed} = this;
        console.log(` ${name} moving to ${topSpeed}`);
    }
}

// 2. Call the constructor with a couple ships, 
// and call accelerate on both of them.
const ship1 = new SpaceShip('King',10);
const ship2 = new SpaceShip('Queen',20);
ship1.accelerate();
ship2.accelerate();

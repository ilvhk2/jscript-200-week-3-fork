// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price

const logReceipt = (...items) => {
  let total = 0; 

  items.forEach( x => { 
    console.log(`  ${x.descr} - \$${x.price}` );
    total += x.price;
   })
   console.log( `----------------------------\n Sub Total = \$${total}`);
   
   let totalPlusTax = total + parseFloat(getTax( total)) ;  
   console.log( `      Tax - \$${getTax( total) } `);
   
   console.log(`Total with Tax= ${totalPlusTax}`) ;
       
};
//EXTRA CREDIT - 1 point for itemized receipt without subtotal and taxes - 2 points if it has subtotal and taxes built in
const SALES_TAXES = 10.1;

const getTax = function getTax(price){ 
  let tax = (price*SALES_TAXES/100).toFixed(2);
  return tax;
}


// Check
logReceipt(
  { descr: 'Burrito', price: 5.99 },
  { descr: 'Chips & Salsa', price: 2.99 },
  { descr: 'Sprite', price: 1.99 }
);
// should log something like:
// Burrito - $5.99
// Chips & Salsa - $2.99
// Sprite - $1.99
// Total - $10.97
